package com.chabtharin.mvvmdemoapp.ui.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.chabtharin.mvvmdemoapp.R;
import com.chabtharin.mvvmdemoapp.data.db.entity.User;
import com.chabtharin.mvvmdemoapp.databinding.ActivityMainBinding;
import com.chabtharin.mvvmdemoapp.ui.adapter.UserAdapter;
import com.chabtharin.mvvmdemoapp.viewmodel.UserViewModel;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    private ActivityMainBinding  binding;
    private UserViewModel userViewModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        UserAdapter userAdapter = new UserAdapter(this);
        userViewModel = new ViewModelProvider(this).get(UserViewModel.class);
        userViewModel.getAllUsers().observe(this, new Observer<List<User>>() {
            @Override
            public void onChanged(List<User> users) {
                userAdapter.setDataSet(users);
                userAdapter.notifyDataSetChanged();
                Log.i("TAG", "onChanged: " + users);
            }
        });
        binding.rcUser.setAdapter(userAdapter);
        binding.rcUser.setLayoutManager(new LinearLayoutManager(this));
        binding.fabAdd.setOnClickListener((view1) -> onFabAddClicked());
    }

    private void onFabAddClicked () {
        Intent intent = new Intent(this, AddDataActivity.class);
        startActivity(intent);
    }
}






