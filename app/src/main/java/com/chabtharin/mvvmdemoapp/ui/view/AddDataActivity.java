package com.chabtharin.mvvmdemoapp.ui.view;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.chabtharin.mvvmdemoapp.data.db.entity.User;
import com.chabtharin.mvvmdemoapp.databinding.ActivityAddDataBinding;
import com.chabtharin.mvvmdemoapp.viewmodel.UserViewModel;

public class AddDataActivity extends AppCompatActivity {
    private ActivityAddDataBinding binding;
    private UserViewModel userViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        userViewModel = new ViewModelProvider(this).get(UserViewModel.class);
        binding = ActivityAddDataBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        binding.mbSave.setOnClickListener(view1 -> onSaveData());
        binding.ibBack.setOnClickListener(view1 -> {
            finish();
        });
        onEnterPressed();
    }

    private void onEnterPressed() {
        binding.edtLastName.setOnKeyListener((view, i, keyEvent) -> {
            if ((KeyEvent.ACTION_DOWN == keyEvent.getAction()) && (KeyEvent.KEYCODE_ENTER == i)) {
                onSaveData();
            }
            return true;
        });
    }

    private void onSaveData() {
        User user = new User(binding.edtFirstName.getText().toString(), binding.edtLastName.getText().toString());
        userViewModel.insertUser(user);
        Toast.makeText(this, "Add new user successfully", Toast.LENGTH_LONG).show();
        finish();
    }
}