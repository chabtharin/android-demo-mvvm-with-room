package com.chabtharin.mvvmdemoapp.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.chabtharin.mvvmdemoapp.data.db.entity.User;
import com.chabtharin.mvvmdemoapp.databinding.UserItemBinding;

import java.util.ArrayList;
import java.util.List;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.UserViewHolder> {
    private UserItemBinding binding;
    private Context mContext;
//    private
    private List<User> mDataSet = new ArrayList<>();
    public UserAdapter(Context mContext) {
        this.mContext = mContext;
    }

    public void setDataSet(List<User> mDataSet) {
        this.mDataSet = mDataSet;
//        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        binding = UserItemBinding.inflate(LayoutInflater.from(mContext), parent, false);
        View view = binding.getRoot();
        return new UserViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull UserViewHolder holder, int position) {
        binding.textFirstName.setText(mDataSet.get(position).getFirstName());
        binding.textLastName.setText(mDataSet.get(position).getLastName());
    }

    @Override
    public int getItemCount() {
        if (mDataSet != null) {
            return mDataSet.size();
        }
        return 0;
    }

    protected static class UserViewHolder extends RecyclerView.ViewHolder {

        public UserViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
