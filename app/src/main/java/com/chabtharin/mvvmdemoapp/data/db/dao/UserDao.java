package com.chabtharin.mvvmdemoapp.data.db.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.chabtharin.mvvmdemoapp.data.db.entity.User;

import java.util.List;
@Dao
public interface UserDao {
    @Query("SELECT * FROM user")
    LiveData<List<User>> getAllUser();
    @Insert
    void insertUser(User user);
}
