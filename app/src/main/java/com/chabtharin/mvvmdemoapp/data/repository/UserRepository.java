package com.chabtharin.mvvmdemoapp.data.repository;

import android.app.Application;

import androidx.lifecycle.LiveData;

import com.chabtharin.mvvmdemoapp.data.db.AppDatabase;
import com.chabtharin.mvvmdemoapp.data.db.dao.UserDao;
import com.chabtharin.mvvmdemoapp.data.db.entity.User;

import java.util.List;

public class UserRepository {
    private UserDao mUserDao;
    private LiveData<List<User>> mAllUsers;
    public UserRepository (Application application) {
        AppDatabase db = AppDatabase.getDatabase(application.getApplicationContext());
        mUserDao = db.userDao();
        mAllUsers = mUserDao.getAllUser();
    }

    public LiveData<List<User>> getAllUsers() {
        return mAllUsers;
    }

    public void insertUser (User user) {
        AppDatabase.databaseWriteExecutor.execute(() -> {
            mUserDao.insertUser(user);
        });
    }
}









